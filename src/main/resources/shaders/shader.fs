#version 410

in vec3 virtualPosition;
in vec4 gl_FragCoord;
 
out vec4 frag_color;
 
#define MAX_ITERATIONS 200
 
 vec4 rgb2hsv(vec3 c)
{
    //vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    //vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    //vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    //float d = q.x - min(q.w, q.y);
    //float e = 1.0e-10;
    //return vec4(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x, 1.0f);
    return vec4(c, 1.0f);
}
 
vec4 get_color()
{
    //float real = ((gl_FragCoord.x / 1800 - 0.5) * 1 + 0) * 5.0;
    //float imag = ((gl_FragCoord.y / 1800 - 0.5) * 1 + 0) * 5.0;
    
    highp float real = virtualPosition.x;
    highp float imag = virtualPosition.y;

    int iterations = 0;
    bool l_4 = false;
	double xcs = real;
	double ycs = imag;
	double yc = 0, xc = 0, xt = 0;
 
 	do {
 		xcs = xc * xc;
 		ycs = yc * yc;
 		xt = (xcs - ycs) + real;
 		yc = (yc * xc * 2) + imag;
 		xc = xt;
 		l_4 = (xcs + ycs) <= 4;
 	} while(l_4 && iterations++ < MAX_ITERATIONS);
 
    if (l_4) {
        return vec4(0.0f, 0.0f, 0.0f, 1.0f);
    } else {
    	double sum = xcs + ycs;
    	double mu = iterations + 1.0f - log(log(abs(float(sum)))) / log(2);
    	double idx = (mu / (MAX_ITERATIONS + 1));
    	int whole = int((idx * 256 * 6) / 256);
    	int part = int(idx * 256 * 6) % 256;
    	float part2 = float(part) / 256.0f;
    	//return vec4(part2, 0.0f, 0.0f, 1.0f);
    	if(whole == 0) return rgb2hsv(vec3(part2, 0.0f, 0.0f));
    	if(whole == 1) return rgb2hsv(vec3(1.0f, part2, 0.0f));
    	if(whole == 2) return rgb2hsv(vec3(1.0f - part2, 1.0f, 0.0f));
    	if(whole == 3) return rgb2hsv(vec3(0.0f, 1.0f, part2));
    	if(whole == 4) return rgb2hsv(vec3(0.0f, 1.0f - part2, 1.0f));
    	if(whole == 5) return rgb2hsv(vec3(0.0f, 0.0f, 1.0f - part2));
    }
  
    return vec4(0.0f, 0.0f, 0.0f, 1.0f);
}
 
void main()
{
    frag_color = get_color();
}