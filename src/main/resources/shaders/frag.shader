in vec2 v_Pos;

void main() {
    if(v_Pos.x > 0.5) {
        gl_FragColor = vec4(0.3, 0.0, 0.3, 1.0);
    } else {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
}