layout(location = 0) in vec2 position;

uniform mat4 u_MVP;

out vec2 v_Pos;

void main() {
    gl_Position = u_MVP * vec4(position, 0, 1.0);
    v_Pos = position;
}