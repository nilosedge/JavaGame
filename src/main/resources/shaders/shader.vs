#version 410

uniform mat4 transformMatrix;

layout (location = 0) in vec3 position;
out vec3 virtualPosition;

void main() {
	gl_Position = transformMatrix * vec4(position, 1);
	//gl_Position = vec4(position, 1);
	virtualPosition = position;
}