package net.nilosplace.JavaGame.helpers;

import static org.lwjgl.opengl.GL11.*;

import java.io.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;
import org.newdawn.slick.opengl.*;
import org.newdawn.slick.util.ResourceLoader;

public class Artist {

	public static final int WIDTH = 1024, HEIGHT = 640;
	//public static final int WIDTH = 1280, HEIGHT = 960;
	
	public static final int TILE_SIZE = 32;
	
	public static void BeginSession() {
		Display.setTitle("Map Editor");
		try {
			Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, WIDTH, HEIGHT, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	
	public static void DrawQuad(float x, float y, float w, float h) {
		x *= TILE_SIZE; y *= TILE_SIZE;
		glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x + w, y);
		glVertex2f(x + w, y + h);
		glVertex2f(x, y + h);
		glEnd();
	}
	
	public static void DrawTexTile(Texture tex, float x, float y, float angle, boolean abs) {
		if(!abs) {
			x *= TILE_SIZE;
			y *= TILE_SIZE;
		}
		tex.bind();
		glTranslatef(x + TILE_SIZE / 2, y + TILE_SIZE / 2, 0);
		glRotatef(angle, 0, 0, 1);
		glTranslatef(-TILE_SIZE / 2, -TILE_SIZE / 2, 0);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(0, 0);
		glTexCoord2f(1, 0);
		glVertex2f(TILE_SIZE, 0);
		glTexCoord2f(1, 1);
		glVertex2f(TILE_SIZE, TILE_SIZE);
		glTexCoord2f(0, 1);
		glVertex2f(0, TILE_SIZE);
		glEnd();
		glLoadIdentity();
	}
	
	public static Texture loadTexture(String path, String type) {
		Texture tex = null;
		InputStream in = ResourceLoader.getResourceAsStream(path);
		try {
			tex = TextureLoader.getTexture(type, in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tex;
	}
	
	public static Texture loadTexName(String name) {
		Texture tex = null;
		//System.out.println("Loading Tex: " + name);
		tex = loadTexture(name + ".png", "PNG");
		return tex;
	}

	public static void DrawTex(Texture tex) {
		tex.bind();
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(0, 0);
		glTexCoord2f(tex.getWidth(), 0);
		glVertex2f(WIDTH, 0);
		glTexCoord2f(tex.getWidth(), tex.getHeight());
		glVertex2f(WIDTH, HEIGHT);
		glTexCoord2f(0, tex.getHeight());
		glVertex2f(0, HEIGHT);
		glEnd();
		glLoadIdentity();
	}
	
	public static void DrawQuadTex(Texture tex, float x, float y, float w, float h) {
		tex.bind();
		glTranslatef(x, y, 0);
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(0, 0);
		glTexCoord2f(tex.getWidth(), 0);
		glVertex2f(w, 0);
		glTexCoord2f(tex.getWidth(), tex.getHeight());
		glVertex2f(w, h);
		glTexCoord2f(0, tex.getHeight());
		glVertex2f(0, h);
		glEnd();
		glLoadIdentity();
	}
}
