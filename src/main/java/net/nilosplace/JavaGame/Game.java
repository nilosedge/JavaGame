package net.nilosplace.JavaGame;

import net.nilosplace.JavaGame.helpers.Artist;
import net.nilosplace.JavaGame.interfaces.Updateable;

public class Game implements Updateable {

	
	int[][] map = new int[Artist.WIDTH / Artist.TILE_SIZE][Artist.HEIGHT / Artist.TILE_SIZE];
	
	TileGrid g = new TileGrid(map);
	Player p = new Player(g);
	ArrowMap am = new ArrowMap(p);
	
	public void update() {
		g.draw();
		p.update();
		
	}

}
