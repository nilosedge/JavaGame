package net.nilosplace.JavaGame;

import org.lwjgl.opengl.Display;

import net.nilosplace.JavaGame.helpers.*;

public class Main {
	
	public Main() {
		System.out.println(System.getProperty("java.library.path"));
		Artist.BeginSession();

		while(!Display.isCloseRequested()) {
			Clock.update();
			
			StateManager.update();
			
			Display.update();
			Display.sync(60);
		}
		
		Display.destroy();
	}
	
	public static void main(String[] args) {
		new Main();
	}
}
