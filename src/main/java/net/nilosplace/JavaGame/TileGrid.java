package net.nilosplace.JavaGame;

import net.nilosplace.JavaGame.enums.TileType;
import net.nilosplace.JavaGame.interfaces.Drawable;


public class TileGrid implements Drawable {

	private int xs = 0;
	private int ys = 0;
	public Tile[][] map;
	
//	public TileGrid() {
//		map = new Tile[Artist.WIDTH / tsX][Artist.HEIGHT / tsY];
//		for(int i = 0; i < map.length; i++) {
//			for(int j = 0; j < map[i].length; j++) {
//				map[i][j] = new Tile(i * tsX, j * tsY, tsX, tsY, TileType.Grass);
//			}
//		}
//	}
	
	public TileGrid(int[][] newMap) {
		xs = newMap.length;
		ys = newMap[0].length;
		
		//System.out.println("NewMap(" + newMap.length + "," + newMap[0].length + ")");
		map = new Tile[xs][ys];
		//System.out.println("Map(" + map.length + "," + map[0].length + ")");
		for(int i = 0; i < map.length; i++) {
			for(int j = 0; j < map[i].length; j++) {
				//System.out.println("Point(" + i + "," + j + ")");
				map[i][j] = new Tile(TileType.lookupTile(newMap[i][j]).tex, i, j, 0);
			}
		}
	}
	
	public void setTile(int x, int y, TileType type) {
		if(x < xs && y < ys) {
			map[x][y] = new Tile(type.tex, x, y, 0);
		}
	}
	
	public Tile getTile(int x, int y) {
		return map[x][y];
	}
	
	public void draw() {
		for(int i = 0; i < map.length; i++) {
			for(int j = 0; j < map[i].length; j++) {
				map[i][j].draw();
			}
		}
	}

	public int getXs() {
		return xs;
	}
	public void setXs(int xs) {
		this.xs = xs;
	}
	public int getYs() {
		return ys;
	}
	public void setYs(int ys) {
		this.ys = ys;
	}
	public Tile[][] getMap() {
		return map;
	}
	public void setMap(Tile[][] map) {
		this.map = map;
	}
}
