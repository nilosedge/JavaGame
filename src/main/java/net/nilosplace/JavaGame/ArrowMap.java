package net.nilosplace.JavaGame;

import net.nilosplace.JavaGame.enums.UnitType;


public class ArrowMap {

	public Tile[][] map;
	public Player p;
	
	public ArrowMap(Player p) {
		this.p = p;
	}
	
	public void update(Tile[][] cm) {

		//System.out.println("NewMap(" + newMap.length + "," + newMap[0].length + ")");
		map = new Tile[cm.length][cm[0].length];
		//System.out.println("Map(" + map.length + "," + map[0].length + ")");

		for(int i = 0; i < map.length; i++) {
			for(int j = 0; j < map[i].length; j++) {
				//System.out.println("Point(" + i + "," + j + ")");
				map[i][j] = new Tile(UnitType.Arrow.tex, cm[i][j].getX(), cm[i][j].getY(), 0);
			}
		}

	}

	public void draw() {
		for(int i = 0; i < map.length; i++) {
			for(int j = 0; j < map[i].length; j++) {
				if(p.getX() != i || p.getY() != j) {
					map[i][j].draw();
				}
			}
		}
	}

}
