package net.nilosplace.JavaGame.enums;

import org.newdawn.slick.opengl.Texture;

import net.nilosplace.JavaGame.helpers.Artist;

public enum TileType {

	Grass(0, true, Artist.loadTexName("grass64")),
	Dirt(1, false, Artist.loadTexName("dirt64")),
	Water(2, false, Artist.loadTexName("water64"))
	;
	
	public int id;
	public boolean build;
	public Texture tex;
	
	TileType(int id, boolean build, Texture tex) {
		this.id = id;
		this.build = build;
		this.tex = tex;
	}

	public static TileType lookupTile(int i) {
		for(TileType t: TileType.values()) {
			if(t.id == i) return t;
		}
		return null;
	}
}
