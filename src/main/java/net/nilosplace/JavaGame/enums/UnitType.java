package net.nilosplace.JavaGame.enums;

import org.newdawn.slick.opengl.Texture;

import net.nilosplace.JavaGame.helpers.Artist;

public enum UnitType {

	Enemy(3, Artist.loadTexName("enemy64")),
	Connon(4, Artist.loadTexName("cannon64")),
	Gun(5, Artist.loadTexName("gun64")),
	Arrow(6, Artist.loadTexName("arrow64"))
	;
	
	public int id;
	public Texture tex;
	
	UnitType(int id, Texture tex) {
		this.id = id;
		this.tex = tex;
	}

	public static UnitType lookupUnit(int i) {
		for(UnitType u: UnitType.values()) {
			if(u.id == i) return u;
		}
		return null;
	}
}
