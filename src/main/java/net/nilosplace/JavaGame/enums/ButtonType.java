package net.nilosplace.JavaGame.enums;

import org.newdawn.slick.opengl.Texture;

import net.nilosplace.JavaGame.helpers.Artist;

public enum ButtonType {
	
	Play(1, Artist.loadTexName("playbutton")),
	Editor(2, Artist.loadTexName("editorbutton")),
	Quit(3, Artist.loadTexName("quitbutton"))
	;
	
	public int id;
	public Texture tex;
	
	ButtonType(int id, Texture tex) {
		this.id = id;
		this.tex = tex;
	}

	public static ButtonType lookupUnit(int i) {
		for(ButtonType b: ButtonType.values()) {
			if(b.id == i) return b;
		}
		return null;
	}
}
