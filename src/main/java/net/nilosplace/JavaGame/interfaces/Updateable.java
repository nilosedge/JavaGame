package net.nilosplace.JavaGame.interfaces;

public interface Updateable {

	public void update();
}
