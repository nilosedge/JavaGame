package net.nilosplace.JavaGame.interfaces;

public interface Drawable {

	public void draw();
}
