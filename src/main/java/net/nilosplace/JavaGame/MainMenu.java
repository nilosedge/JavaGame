package net.nilosplace.JavaGame;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;

import net.nilosplace.JavaGame.UI.UI;
import net.nilosplace.JavaGame.enums.*;
import net.nilosplace.JavaGame.helpers.Artist;
import net.nilosplace.JavaGame.interfaces.Updateable;

public class MainMenu implements Updateable {

	private Texture background;
	private UI ui;
	
	public MainMenu() {
		background = Artist.loadTexName("mainmenu");
		ui = new UI();
		ui.addButton("Play", ButtonType.Play.tex, Artist.WIDTH / 2 - 128, (int) (Artist.HEIGHT * 0.35f));
		ui.addButton("Editor", ButtonType.Editor.tex, Artist.WIDTH / 2 - 128, (int) (Artist.HEIGHT * 0.55f));
		ui.addButton("Quit", ButtonType.Quit.tex, Artist.WIDTH / 2 - 128, (int) (Artist.HEIGHT * 0.75f));
		
	}
	
	public void updateButtons() {
		if(Mouse.isButtonDown(0)) {
			if(ui.isButtonClicked("Play")) {
				StateManager.setState(GameState.Game);
			} else if(ui.isButtonClicked("Editor")) {
				StateManager.setState(GameState.Editor);
			} else if(ui.isButtonClicked("Quit")) {
				System.exit(0);
			}
		}
	}

	public void update() {
		Artist.DrawTex(background);
		ui.draw();
		updateButtons();
	}

}
