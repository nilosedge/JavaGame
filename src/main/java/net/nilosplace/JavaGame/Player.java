package net.nilosplace.JavaGame;

import org.lwjgl.input.*;
import org.newdawn.slick.opengl.Texture;

import net.nilosplace.JavaGame.enums.*;
import net.nilosplace.JavaGame.helpers.Artist;
import net.nilosplace.JavaGame.interfaces.*;

public class Player implements Drawable, Updateable {

	private TileGrid grid;
    private int index;
    private Texture tex = UnitType.Connon.tex;
	private float x = 0;
	private float y = 0;
    
    public Player(TileGrid grid) {
		this.grid = grid;
		this.index = 0;
	}
	
	public void setTile() {
		grid.setTile(
				(int) Math.floor(Mouse.getX() / Artist.TILE_SIZE), 
				(int) Math.floor((Artist.HEIGHT - Mouse.getY() - 1) / Artist.TILE_SIZE),
				TileType.lookupTile(index)
		);
	}
	
	public void update() {
		//System.out.println(Mouse.getEventButtonState());
		if (Mouse.isButtonDown(0)) { // && Mouse.getEventButtonState()) {
			setTile();
			System.out.println("Down");
		}
		//System.out.println(Mouse.getEventButtonState());
		float speed = (128 / Artist.TILE_SIZE);
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			x += speed;
			if(x > Artist.WIDTH) x = 0;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			x -= speed;
			if(x < 0) x = (Artist.WIDTH);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			y -= speed;
			if(y < 0) y = (Artist.HEIGHT);
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			y += speed;
			if(y > Artist.HEIGHT) y = 0;
		}
		
		while (Keyboard.next()) {
			if (Keyboard.getEventKey() == Keyboard.KEY_RSHIFT && Keyboard.getEventKeyState()) {
				System.out.println("Index: " + index);
				index = ((index + 1) % TileType.values().length);
			}
		}
		
	}
	
	public void draw() {
		if((y > (Artist.HEIGHT - Artist.TILE_SIZE)) && (x > (Artist.WIDTH - Artist.TILE_SIZE))) {
			Artist.DrawTexTile(tex, -(Artist.WIDTH - x), -(Artist.HEIGHT - y), 0, true);
		}
		if(x > (Artist.WIDTH - Artist.TILE_SIZE)) {
			Artist.DrawTexTile(tex, -(Artist.WIDTH - x), y, 0, true);	
		}
		if(y > (Artist.HEIGHT - Artist.TILE_SIZE)) {
			Artist.DrawTexTile(tex, x, -(Artist.HEIGHT - y), 0, true);
		}
		Artist.DrawTexTile(tex, x, y, 0, true);
	}

	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
}
