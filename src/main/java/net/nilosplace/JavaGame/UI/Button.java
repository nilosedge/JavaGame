package net.nilosplace.JavaGame.UI;

import org.newdawn.slick.opengl.Texture;

public class Button {

	private Texture texture;
	private int x, y, w, h;
	private String name;

	public Button(String name, Texture texture, int x, int y, int w, int h) {
		this.name = name;
		this.texture = texture;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	public Button(String name, Texture texture, int x, int y) {
		this.name = name;
		this.texture = texture;
		this.x = x;
		this.y = y;
		this.w = texture.getImageWidth();
		this.h = texture.getImageHeight();
		
	}

	
	public Texture getTexture() {
		return texture;
	}
	public void setTexture(Texture texture) {
		this.texture = texture;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getW() {
		return w;
	}
	public void setW(int w) {
		this.w = w;
	}
	public int getH() {
		return h;
	}
	public void setH(int h) {
		this.h = h;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
