package net.nilosplace.JavaGame.UI;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;

import net.nilosplace.JavaGame.helpers.Artist;

public class UI {

	ArrayList<Button> buttons;
	
	public UI() {
		buttons = new ArrayList<Button>();
	}
	
	public void addButton(String name, Texture texture, int x, int y) {
		buttons.add(new Button(name, texture, x, y));

	}
	
	public boolean isButtonClicked(String name) {
		Button b = getButton(name);
		float my = Artist.HEIGHT - Mouse.getY() - 1;
		if(Mouse.getX() > b.getX() && Mouse.getX() < b.getX() + b.getW() && my > b.getY() && my < b.getY() + b.getH()) {
			return true;
		}
		return false;
	}
	
	private Button getButton(String name) {
		for(Button b: buttons) {
			if(b.getName().equals(name)) {
				return b;
			}
		}
		return null;
	}
	
	public void draw() {
		for(Button b: buttons) {
			Artist.DrawQuadTex(b.getTexture(), b.getX(), b.getY(), b.getW(), b.getH());
		}
	}
}
