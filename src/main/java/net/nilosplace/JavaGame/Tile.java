package net.nilosplace.JavaGame;

import org.newdawn.slick.opengl.Texture;

import net.nilosplace.JavaGame.helpers.Artist;
import net.nilosplace.JavaGame.interfaces.Drawable;

public class Tile implements Drawable {

	private float x, y, rot;
	private Texture texture;
	
	public Tile(Texture texture, float x, float y, float rot) {
		this.x = x;
		this.y = y;
		this.texture = texture;
	}
	
	public void draw() {
		Artist.DrawTexTile(texture, x, y, rot, false);
	}

	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
}
