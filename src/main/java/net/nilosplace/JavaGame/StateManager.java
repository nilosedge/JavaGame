package net.nilosplace.JavaGame;

import net.nilosplace.JavaGame.enums.GameState;

public class StateManager {
	
	public static MainMenu mainMenu;
	public static Editor editor;
	public static Game game;
	
	public static GameState state = GameState.MainMenu;
	
	public static void update() {
		switch (state) {
		case Editor:
			if(editor == null) {
				editor = new Editor();
			}
			editor.update();
			break;
		case Game:
			if(game == null) {
				game = new Game();
			}
			game.update();
			break;
		case MainMenu:
			if(mainMenu == null) {
				mainMenu = new MainMenu();
			}
			mainMenu.update();
			break;

		default:
			break;
		}
	}
	
	public static void setState(GameState state) {
		StateManager.state = state;
	}

}
