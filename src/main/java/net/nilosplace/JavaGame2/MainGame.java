package net.nilosplace.JavaGame2;

import java.io.IOException;

import org.newdawn.slick.*;
import org.newdawn.slick.particles.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.util.Log;

public class MainGame extends BasicGameState {

	private TiledMap gameMap;
	private ParticleSystem system;
	private ConfigurableEmitter emitter;
	private float x = 34f, y = 34f;

	public void init(GameContainer arg0, StateBasedGame arg1) throws SlickException {
		//loadEmitter();
		
		Log.setVerbose(true);
		gameMap = new TiledMap("water.tmx");
		try {
			emitter = ParticleIO.loadEmitter("particle.xml");
			//emitter.setPosition(150, 150);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void render(GameContainer arg0, StateBasedGame arg1, Graphics arg2) throws SlickException {
		gameMap.render((int)x, (int)y);
		if(system != null) {
			system.render(x, y);
		}
	}

	public void update(GameContainer container, StateBasedGame arg1, int delta) throws SlickException {
		Input input = container.getInput();

		if (input.isKeyDown(Input.KEY_UP)) {
			y += delta * 0.5f;
		}
		if (input.isKeyDown(Input.KEY_DOWN)) {
			y -= delta * 0.5f;
		}
		if (input.isKeyDown(Input.KEY_LEFT)) {
			x += delta * 0.5f;
		}
		if (input.isKeyDown(Input.KEY_RIGHT)) {
			x -= delta * 0.5f;
		}
		if(input.isKeyDown(Input.KEY_R)) {
			loadEmitter();
		}
		if(input.isKeyDown(Input.KEY_S)) {
			stopEmitter();
		}
		
		if(system != null) {
			system.update(delta);
		}
	}

	private void stopEmitter() {
		system = null;
	}
	
	private void loadEmitter() {
		try {
			Image image = new Image("particle.png", false);
			system = new ParticleSystem(image, 1500);
			system.addEmitter(emitter);
			system.setBlendingMode(ParticleSystem.BLEND_ADDITIVE);
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public int getID() {
		return 0;
	}
}
