package net.nilosplace.JavaGame2;


import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.io.*;
import java.nio.*;

import org.joml.*;
import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryStack;

public class Application {

    // The window handle
    private long window;

    // Projection Matrix
    private Matrix4f pmatrix = new Matrix4f().ortho(-2.0f, 2.0f, -2.0f, 2.0f, -1.0f, 1.0f);

    // Camera Panning Input
    private boolean upArrow = false;
    private boolean downArrow = false;
    private boolean rightArrow = false;
    private boolean leftArrow = false;

    // Zooming
    private boolean zoomingIn = false;
    private boolean zoomingOut = false;

    public void run() {
        System.out.println("Hello LWJGL " + Version.getVersion() + "!");

        init();
        loop();

        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    private void init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        // Create the window
        window = glfwCreateWindow(960 / 2, 960 / 2, "Hello World!", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE ) {
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
            }

            // Up Arrow Key
            if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
                upArrow = true;
            }
            if(key == GLFW_KEY_UP && action == GLFW_RELEASE) {
                upArrow = false;
            }

            // Down Arrow Key
            if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
                downArrow = true;
            }
            if(key == GLFW_KEY_DOWN && action == GLFW_RELEASE) {
                downArrow = false;
            }

            // Left Arrow Key
            if (key == GLFW_KEY_LEFT && action == GLFW_PRESS) {
                leftArrow = true;
            }
            if(key == GLFW_KEY_LEFT && action == GLFW_RELEASE) {
                leftArrow = false;
            }

            // Right Arrow Key
            if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS) {
                rightArrow = true;
            }
            if(key == GLFW_KEY_RIGHT && action == GLFW_RELEASE) {
                rightArrow = false;
            }

            // Zooming In (Z Key)
            if (key == GLFW_KEY_Z && action == GLFW_PRESS) {
                zoomingIn = true;
            }
            if (key == GLFW_KEY_Z && action == GLFW_RELEASE) {
                zoomingIn = false;
            }

            // Zooming Out (X Key)
            if (key == GLFW_KEY_X && action == GLFW_PRESS) {
                zoomingOut = true;
            }
            if (key == GLFW_KEY_X && action == GLFW_RELEASE) {
                zoomingOut = false;
            }

        });

        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);
    }

    private void loop() {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities();

        // Set the clear color
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        // Vertices - Positions
        float[] vertices = new float[] {
                -1.0f,  1.0f,   // Vertex 0
                -1.0f, -1.0f,   // Vertex 1
                 1.0f, -1.0f,   // Vertex 2
                 1.0f,  1.0f    // Vertex 3
        };

        // VBO (Vertex Buffer Object)
        FloatBuffer vboBuffer = BufferUtils.createFloatBuffer(vertices.length);
        for(float vertex : vertices) {
            vboBuffer.put(vertex);
        }
        vboBuffer.flip();

        // Pass data to GPU
        int positionElementCount = vertices.length / 4;
        int vboID = GL30.glGenBuffers();
        GL30.glBindBuffer(GL30.GL_ARRAY_BUFFER, vboID);
        GL30.glBufferData(GL30.GL_ARRAY_BUFFER, vboBuffer, GL30.GL_STATIC_DRAW);
        GL30.glVertexAttribPointer(0, positionElementCount, GL_FLOAT, false, positionElementCount * Float.BYTES, 0);
        GL30.glEnableVertexAttribArray(0);

        // Indices
        int[] indices = new int[] {
            0, 1, 2,
            2, 3, 0
        };

        // IBO (Index Buffer Object)
        IntBuffer iboBuffer = BufferUtils.createIntBuffer(indices.length);
        for(int index : indices) {
            iboBuffer.put(index);
        }
        iboBuffer.flip();

        // Pass data to GPU
        int iboID = GL30.glGenBuffers();
        GL30.glBindBuffer(GL30.GL_ELEMENT_ARRAY_BUFFER, iboID);
        GL30.glBufferData(GL30.GL_ELEMENT_ARRAY_BUFFER, iboBuffer, GL30.GL_STATIC_DRAW);

        // Shaders
        int programID = GL30.glCreateProgram();
        int vertShaderObj = GL30.glCreateShader(GL30.GL_VERTEX_SHADER);
        int fragShaderObj = GL30.glCreateShader(GL30.GL_FRAGMENT_SHADER);
        String vertexShader = parseShaderFromFile("/shaders/vert.shader");
        GL30.glShaderSource(vertShaderObj, vertexShader);
        GL30.glCompileShader(vertShaderObj);
        String fragmentShader = parseShaderFromFile("/shaders/frag.shader");
        GL30.glShaderSource(fragShaderObj, fragmentShader);
        GL30.glCompileShader(fragShaderObj);
        GL30.glAttachShader(programID, vertShaderObj);
        GL30.glAttachShader(programID, fragShaderObj);
        GL30.glLinkProgram(programID);
        GL30.glValidateProgram(programID);
        GL30.glUseProgram(programID);

        float basePanningSpeed = 0.0125f;
        float currentPanningSpeed = 0.0125f;
        float zoomAmount = 1.0f;
        float zoomSpeed = 1.05f;

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while ( !glfwWindowShouldClose(window) ) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

            // Camera Panning
            if(upArrow) {
                // Translate the projection matrix by the distance cameraSpeed in the positive y direction
                pmatrix.translate(new Vector3f(0.0f, currentPanningSpeed, 0.0f));
            }
            if(downArrow) {
                // Translate the projection matrix by the distance cameraSpeed in the negative y direction
                pmatrix.translate(new Vector3f(0.0f, -currentPanningSpeed, 0.0f));
            }
            if(rightArrow) {
                // Translate the projection matrix by the distance cameraSpeed in the positive x direction
                pmatrix.translate(new Vector3f(currentPanningSpeed, 0.0f, 0.0f));
            }
            if(leftArrow) {
                // Translate the projection matrix by the distance cameraSpeed in the negative x direction
                pmatrix.translate(new Vector3f(-currentPanningSpeed, 0.0f, 0.0f));
            }

            // Zooming
            if(zoomingIn) {
                zoomAmount = zoomAmount * zoomSpeed; // Update the zoomAmount
                currentPanningSpeed = basePanningSpeed / zoomAmount; // Update camera panning speed

                pmatrix.scaleLocal(zoomSpeed); // Zoom In

            } else if(zoomingOut) {
                zoomAmount = zoomAmount * (1 / zoomSpeed); // Update the zoomAmount
                currentPanningSpeed = basePanningSpeed / zoomAmount; // Update camera panning speed

                pmatrix.scaleLocal(1 / zoomSpeed); // Zoom Out

            }

            // Camera
            try (MemoryStack stack = MemoryStack.stackPush()) {
                FloatBuffer projection = pmatrix.get(stack.mallocFloat(4 * 4));
                int mat4location = GL30.glGetUniformLocation(programID, "u_MVP");
                GL30.glUniformMatrix4fv(mat4location, false, projection);
            }

            GL30.glDrawElements(GL30.GL_TRIANGLES, indices.length, GL_UNSIGNED_INT, 0); //Draw our square

            glfwSwapBuffers(window); // swap the color buffers

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents();
        }
    }

    private static String parseShaderFromFile(String filePath) {
        StringBuilder data = new StringBuilder();
        String line = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(Application.class.getResourceAsStream(filePath)));
            line = reader.readLine();
            while( line != null )
            {
                data.append(line);
                data.append('\n');
                line = reader.readLine();
            }
        }
        catch(Exception e)
        {
            throw new IllegalArgumentException("Unable to load shader from: " + filePath, e);
        }

        return data.toString();
    }

    public static void main(String[] args) {
        new Application().run();
    }

}