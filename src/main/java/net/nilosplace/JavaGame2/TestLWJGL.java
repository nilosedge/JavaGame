package net.nilosplace.JavaGame2;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_POINTS;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glPointSize;
import static org.lwjgl.opengl.GL11C.GL_BLEND;
import static org.lwjgl.opengl.GL11C.glClearColor;
import static org.lwjgl.opengl.GL11C.glDrawArrays;
import static org.lwjgl.opengl.GL11C.glEnable;
import static org.lwjgl.opengl.GL11C.glViewport;
import static org.lwjgl.opengl.GL20C.glGetUniformLocation;
import static org.lwjgl.opengl.GL30C.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.*;

import java.awt.Color;
import java.nio.*;

import org.joml.Matrix4f;
import org.lwjgl.Version;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

public class TestLWJGL {

	// The window handle
	private long window;
	
	private Matrix4f pmatrix = new Matrix4f().ortho(-2.0f, 2.0f, -2.0f, 2.0f, -1.0f, 1.0f);

	// Camera Panning Input
    private boolean upArrow = false;
    private boolean downArrow = false;
    private boolean rightArrow = false;
    private boolean leftArrow = false;

    // Zooming
    private boolean zoomingIn = false;
    private boolean zoomingOut = false;
	
	private Color[] colors = new Color[256*6];

	private double sx = 559912.9782739371;
	private double sy = 734885.7839845422;
	private double tx = 782546.677600376;
	private double ty = 14111.51836547885;

	private int w = 1600;
	private int h = 1200;
	private int hw = w / 2;
	private int hh = h / 2;
	private boolean mouse1Pressed = false;
	private boolean mouse2Pressed = false;
	private double ix, itx;
	private double iy, ity;
	
	private byte[] fire = new byte[w*h*3];
	private ByteBuffer pixels;

	public void run() {
		System.out.println("Hello LWJGL " + Version.getVersion() + "!");

		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		pixels = ByteBuffer.allocateDirect(w*h*3).put(fire);
        pixels.flip();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

		// Create the window
		window = glfwCreateWindow(w, h, "Hello World!", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		GLFWKeyCallback keyCallback;
		GLFWFramebufferSizeCallback fbCallback;
		Callback debugProc;

		glfwSetFramebufferSizeCallback(window, fbCallback = new GLFWFramebufferSizeCallback() {
			public void invoke(long window, int width, int height) {
				if (width > 0 && height > 0 && (w != width || h != height)) {
					w = width;
					h = height;
				}
			}
		});

		glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
					glfwSetWindowShouldClose(window, true);
			}
		});

		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(window, (vidmode.width() - w) / 2, (vidmode.height() - h) / 2);

		try (MemoryStack frame = stackPush()) {
			IntBuffer framebufferSize = frame.mallocInt(2);
			nglfwGetFramebufferSize(window, memAddress(framebufferSize), memAddress(framebufferSize) + 4);
			w = framebufferSize.get(0);
			h = framebufferSize.get(1);
		}

		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);
		glfwShowWindow(window);

		GL.createCapabilities();
		debugProc = GLUtil.setupDebugMessageCallback();

		// generate planet/cloud mesh
		//        ParShapesMesh mesh = par_shapes_create_subdivided_sphere(4);
		int vao = glGenVertexArrays();
		//        int numIndices = mesh.ntriangles() * 3;
		glBindVertexArray(vao);
		//        int vbo = glGenBuffers();
		//        int ebo = glGenBuffers();
		//        glBindBuffer(GL_ARRAY_BUFFER, vbo);
		//        glBufferData(GL_ARRAY_BUFFER, mesh.points(mesh.npoints() * 3), GL_STATIC_DRAW);
		//        glVertexAttribPointer(0, 3, GL_FLOAT, false, 3 * 4, 0L);
		//        glEnableVertexAttribArray(0);
		//        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		//        glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.triangles(numIndices), GL_STATIC_DRAW);
		//        par_shapes_free_mesh(mesh);

		// set global GL state
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		//glEnable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		// glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// glEnable(GL_PROGRAM_POINT_SIZE);

		glDrawArrays(GL_POINTS, 0, 4); 

		MandelShader shader = new MandelShader();

		int program = shader.program;

		GL30C.glUseProgram(program);

		while ( !glfwWindowShouldClose(window) ) {
			glfwPollEvents();
			//glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
			glViewport(0, 0, w, h);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			try (MemoryStack stack = stackPush()) {
				int dataUniform = glGetUniformLocation(program, "data");
				GL30C.glUniform4f(dataUniform, (float)tx, (float)ty, (float)sx, (float)sy);
			}
			glPointSize(2);

			
	        
			//drawFractal();

			glfwSwapBuffers(window); // swap the color buffers
			// Poll for window events. The key callback above will only be
			// invoked during this call.

		}

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}

	private void init() {

		glfwSetCursorPosCallback(window, (window, x, y) -> {
			if(mouse1Pressed) {
				if(ix == 0.0) {
					ix = x;
					itx = tx;
				}
				if(iy == 0.0) {
					iy = y;
					ity = ty;
				}
				tx = (itx + (x - ix));
				ty = (ity + (y - iy));

			} else if(mouse2Pressed) {
				if(ix == 0.0) {
					ix = x;
					itx = tx;
					tx -= (x - hw);
				}
				if(iy == 0.0) {
					iy = y;
					ity = ty;
					ty -= ((double)y - hh);
				}

				double xx = (tx - hw) / sx;
				sx /= 1.2;
				tx = ((xx * sx) + hw);

				double yy = (ty - hh) / sy;
				sy /= 1.2;
				ty = ((yy * sy) + hh);


			} else {
				ix = 0.0;
				iy = 0.0;
			}
			//System.out.println("X: " + x + " Y: " + y);
			//System.out.println("SX: " + sx + " SY: " + sy);
		});


		glfwSetMouseButtonCallback(window, (window, arg1, arg2, arg3) -> {
			if(arg1 == 0 && arg2 == 1) {
				mouse1Pressed = true;
			} else mouse1Pressed = false;

			if(arg1 == 1 && arg2 == 1) {
				mouse2Pressed = true;
			} else mouse2Pressed = false;

			System.out.println("Mouse Event: " + arg1 + " " + arg2 + " " + arg3);
		});

		for(int i = 0; i < 256; i++) {
			colors[i + (256 * 0)] = new Color(i, 0, 0);
			colors[i + (256 * 1)] = new Color(255, i, 0);
			colors[i + (256 * 2)] = new Color(255-i, 255, 0);
			colors[i + (256 * 3)] = new Color(0, 255, i);
			colors[i + (256 * 4)] = new Color(0, 255-i, 255);
			colors[i + (256 * 5)] = new Color(0, 0, 255-i);
		}

	}


	private void drawFractal() {
		//glPointSize(2);
		//double x = 0; double y = 0;
		//double dx = 0; double dy = 0;

		//		glBegin(GL_POINTS);
		//		{
		//			for(int i = 0; i < w; i++) {
		//				for(int j = 0; j < h; j++) {
		//					x = ((double)i - tx) / sx;
		//					y = ((double)j - ty) / sy;
		//
		//					Color color = computeFractal(x, y);
		//					glColor3f((float)color.getRed() / (float)256, (float)color.getGreen() / (float)256, (float)color.getBlue() / (float)256);
		//					dx = ((double)i - (double)hw) / (double)hw;
		//					dy = ((double)j - (double)hh) / (double)hh;
		//
		//					glVertex2f((float)dx, (float)-dy);
		//				}
		//			}
		//		}
		//		glEnd();
		//System.out.println("Draw");
	}

	public Color computeFractal(double in_x, double in_y) {

		double xc = 0;
		double yc = 0;
		double xt = 0;

		int iteration = 0;
		int max_iteration = 250;
		boolean lessThenFour = false;
		double xcs = 0;
		double ycs = 0;

		do {
			xcs = xc * xc;
			ycs = yc * yc;
			xt = (xcs - ycs) + in_x;
			yc = (yc * xc * 2) + in_y;
			xc = xt;
			lessThenFour = (xcs + ycs) <= 4;
		} while(lessThenFour && iteration++ < max_iteration);

		//System.out.println("Mu: " + mu);
		if(lessThenFour) {
			return Color.BLACK;
		} else {
			double mu = iteration + 1 - Math.log(Math.log(Math.abs(xcs + ycs))) / Math.log(2);
			return colors[(int)((mu / ((double)max_iteration + 1)) * colors.length)];
			//			int r = (int)((mu / ((double)max_iteration + 1)) * (double)256);
			//			int g = (256 - r);
			//			return new Color(g,g,g);
		}
	}

	public static void main(String[] args) {
		new TestLWJGL().run();
	}

}
