package net.nilosplace.JavaGame2.simple;

import static org.lwjgl.opengl.GL20C.*;

import java.io.*;

public class Shader {
	public int program;
	private int vs;
	private int fs;
	
	public Shader(String path, String filename) {
		
		vs = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vs, readFile(path, filename + ".vs"));
		glCompileShader(vs);
		if(glGetShaderi(vs, GL_COMPILE_STATUS) != 1) {
			System.err.println("VS: " + glGetShaderInfoLog(vs));
			System.exit(-1);
		}
		
		fs = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fs, readFile(path, filename + ".fs"));
		glCompileShader(fs);
		if(glGetShaderi(fs, GL_COMPILE_STATUS) != 1) {
			System.err.println("FS: " + glGetShaderInfoLog(fs));
			System.exit(-1);
		}

		program = glCreateProgram();
		glAttachShader(program, vs);
		glAttachShader(program, fs);
		
		//glBindAttribLocation(program, 0, "vertices");

		glLinkProgram(program);
		if(glGetProgrami(program, GL_LINK_STATUS) != 1) {
			System.err.println("LINK: " + glGetProgramInfoLog(program));
			System.exit(-1);
		}
		glValidateProgram(program);
		
		if(glGetProgrami(program, GL_VALIDATE_STATUS) != 1) {
			System.err.println("VALID: " + glGetProgramInfoLog(program));
			System.exit(-1);
		}
	}

	public void bind() {
		glUseProgram(program);
	}

	private String readFile(String path, String filename) {
		StringBuilder string = new StringBuilder();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(path + "/shaders/" + filename));
			String line;
			while((line = reader.readLine()) != null) {
				string.append(line + "\n");
			}
			reader.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return string.toString();
	}

}
