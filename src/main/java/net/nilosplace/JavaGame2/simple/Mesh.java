package net.nilosplace.JavaGame2.simple;

import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL33C;

public class Mesh {

	private int vao;
	private int vbo;
	private int ibo;

	private int vc;
	private int ic;

	public Mesh() {

		vao = GL33C.glGenVertexArrays();
		vbo = GL33C.glGenBuffers();

		GL33C.glBindVertexArray(vao);

		GL33C.glBindBuffer(GL33C.GL_ARRAY_BUFFER, vbo);
		GL33C.glVertexAttribPointer(0, 3, GL33C.GL_FLOAT, false, 0, 0);
		GL33C.glEnableVertexAttribArray(0);
	}

	public boolean create(float verts[], int[] indices) {


		GL33C.glBufferData(GL33C.GL_ARRAY_BUFFER, verts, GL33C.GL_STATIC_DRAW);


		//position = glGetAttribLocation(shader, "position")

		GL33C.glVertexAttribPointer(0, vc, GL33C.GL_FLOAT, false, vc * Float.BYTES, 0);
		GL33C.glEnableVertexAttribArray(0);


		IntBuffer iboBuffer = BufferUtils.createIntBuffer(indices.length);
		for(int index : indices) {
			iboBuffer.put(index);
		}
		iboBuffer.flip();
		ic = indices.length;
		// Pass data to GPU
		ibo = GL33C.glGenBuffers();
		GL33C.glBindBuffer(GL33C.GL_ELEMENT_ARRAY_BUFFER, ibo);
		GL33C.glBufferData(GL33C.GL_ELEMENT_ARRAY_BUFFER, iboBuffer, GL33C.GL_STATIC_DRAW);





		//GL33C.glBindBuffer(GL33C.GL_ARRAY_BUFFER, 0);
		//GL33C.glBindVertexArray(0);



		return true;
	}

	public void destroy() {
		GL33C.glDeleteBuffers(vbo);
		GL33C.glDeleteVertexArrays(vao);
	}

	public void draw() {
		GL33C.glBindVertexArray(vao);

		GL33C.glEnableVertexAttribArray(0);

		GL33C.glDrawArrays(GL33C.GL_TRIANGLES, 0, vc);
		GL33C.glDrawElements(GL33C.GL_TRIANGLES, ic, GL33C.GL_UNSIGNED_INT, 0);

		GL33C.glDisableVertexAttribArray(0);

		GL33C.glBindVertexArray(0);
	}
}
