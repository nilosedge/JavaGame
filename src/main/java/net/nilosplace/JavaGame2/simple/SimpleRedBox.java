package net.nilosplace.JavaGame2.simple;

import static org.lwjgl.glfw.GLFW.*;

import java.nio.FloatBuffer;

import org.joml.*;
import org.lwjgl.Version;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.MemoryStack;

public class SimpleRedBox {

	private Matrix4d pmatrix = new Matrix4d().ortho(-2.0f, 2.0f, -2.0f, 2.0f, -1.0f, 1.0f);

	// Camera Panning Input
	private boolean upArrow = false;
	private boolean downArrow = false;
	private boolean rightArrow = false;
	private boolean leftArrow = false;

	// Zooming
	private boolean zoomingIn = false;
	private boolean zoomingOut = false;

	private float basePanningSpeed = 0.025f;
	private float currentPanningSpeed = 0.025f;
	private float zoomAmount = 1.0f;
	private float zoomSpeed = 1.05f;

	public static void main(String[] args) {
		new SimpleRedBox();
	}

	public SimpleRedBox() {

		int w = 1280;
		int h = 960;

		System.out.println("Hello LWJGL " + Version.getVersion() + "!");
		GLFWErrorCallback.createPrint(System.err).set();

		if(!GLFW.glfwInit()) {
			System.err.println("Failed to init");
			System.exit(1);
		}
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL33C.GL_TRUE);
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

		long win = GLFW.glfwCreateWindow(w, h, "Window", 0, 0);

		GLFW.glfwMakeContextCurrent(win);

		GLFWVidMode videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(win, (videoMode.width() - w) / 2, (videoMode.height() - h) / 2);

		glfwSwapInterval(1);
		glfwShowWindow(win);

		GL.createCapabilities();
		GL33C.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		//GL33C.glEnable(GL33C.GL_TEXTURE_2D);
		//GL33C.glViewport(0, 0, w, h);

		//Model model = new Model(vertices, texture, indices);

		GLFWKeyCallback keyCallback;
		glfwSetKeyCallback(win, keyCallback = new GLFWKeyCallback() {
			public void invoke(long window, int key, int scancode, int action, int mods) {
				if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
					glfwSetWindowShouldClose(window, true);
				
				Boolean key_press = null;
				if(action == GLFW_PRESS) key_press = true;
				if(action == GLFW_RELEASE) key_press = false;
				//System.out.println(action + " " + key_press);
				if(key_press != null) {
					switch(key) {
						case GLFW_KEY_UP: upArrow = key_press; break;
						case GLFW_KEY_DOWN: downArrow = key_press; break;
						case GLFW_KEY_LEFT: leftArrow = key_press; break;
						case GLFW_KEY_RIGHT: rightArrow = key_press; break;
						case GLFW_KEY_X: zoomingIn = key_press; break;
						case GLFW_KEY_Z: zoomingOut = key_press; break;
					}
					
				}
			}
		});

		Mesh mesh = new Mesh();
		mesh.create(
			new float[] {
				-3f, -3f, 0, // 0
				-3f, 3f, 0,  // 1
				3f, -3f, 0,  // 2
				3f, 3f, 0,   // 3
			}, 
			new int[] {
				0, 1, 3,
				0, 2, 3,
			}	
		);

		Shader shader = new Shader("/Users/olinblodgett/git/JavaGame/src/main/resources", "shader");

		while(!GLFW.glfwWindowShouldClose(win)) {
			GLFW.glfwPollEvents();

			GL33C.glClear(GL33C.GL_COLOR_BUFFER_BIT);

			//GL30.glDrawElements(GL30.GL_TRIANGLES, indices.length, GL_UNSIGNED_INT, 0);

			handleInput();

			// Camera
			try (MemoryStack stack = MemoryStack.stackPush()) {
				FloatBuffer projection = pmatrix.get(stack.mallocFloat(4 * 4));
				int mat4location = GL30.glGetUniformLocation(shader.program, "transformMatrix");
				GL30.glUniformMatrix4fv(mat4location, false, projection);
			}
			
			shader.bind();
			mesh.draw();
			//model.render();

			GLFW.glfwSwapBuffers(win);
		}
		
		mesh.destroy();

		GLFW.glfwTerminate();

	}

	private void handleInput() {
		// Camera Panning
		if(upArrow) {
			// Translate the projection matrix by the distance cameraSpeed in the positive y direction
			pmatrix.translate(new Vector3f(0.0f, -currentPanningSpeed, 0.0f));
		}
		if(downArrow) {
			// Translate the projection matrix by the distance cameraSpeed in the negative y direction
			pmatrix.translate(new Vector3f(0.0f, currentPanningSpeed, 0.0f));
		}
		if(rightArrow) {
			// Translate the projection matrix by the distance cameraSpeed in the positive x direction
			pmatrix.translate(new Vector3f(-currentPanningSpeed, 0.0f, 0.0f));
		}
		if(leftArrow) {
			// Translate the projection matrix by the distance cameraSpeed in the negative x direction
			pmatrix.translate(new Vector3f(currentPanningSpeed, 0.0f, 0.0f));
		}

		// Zooming
		if(zoomingIn) {
			zoomAmount = zoomAmount * zoomSpeed; // Update the zoomAmount
			currentPanningSpeed = basePanningSpeed / zoomAmount; // Update camera panning speed

			pmatrix.scaleLocal(zoomSpeed); // Zoom In

		} else if(zoomingOut) {
			zoomAmount = zoomAmount * (1 / zoomSpeed); // Update the zoomAmount
			currentPanningSpeed = basePanningSpeed / zoomAmount; // Update camera panning speed

			pmatrix.scaleLocal(1 / zoomSpeed); // Zoom Out

		}

		//System.out.println(pmatrix);

	}

}
