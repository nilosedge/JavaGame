package net.nilosplace.JavaGame2;

import org.newdawn.slick.*;
import org.newdawn.slick.state.StateBasedGame;

public class Main extends StateBasedGame {

	public Main(String name) {
		super(name);
		this.addState(new MainGame());
	}

	public static final int play = 0;

	public static void main(String[] args) {
		AppGameContainer appgc;
		try{
			appgc = new AppGameContainer(new Main("My First Real Game"));
			appgc.setDisplayMode(1200, 800, false);
			//appgc.setAlwaysRender(true);
			//appgc.setVerbose(true);
			//appgc.setResizable(true);
			//appgc.setTargetFrameRate(60);
			appgc.start();
		}catch(SlickException e){
			e.printStackTrace();
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		this.getState(play).init(gc, this);
		this.enterState(play);

	}

}
