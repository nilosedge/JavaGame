package net.nilosplace.JavaGame2.boids;

import java.awt.*;
import java.util.*;
import java.util.List;

import algs.model.*;
import algs.model.kdtree.*;
import algs.model.nd.Hypercube;
import algs.model.twod.TwoDPoint;
import lombok.Data;

@Data
public class Boid implements IMultiPoint {

	private int id;
	private double x;
	private double y;
	private int w;
	private int h;
	private double dx;
	private double dy;
	private int flockId;
	private int size = 4;
	private int margin = 50;
	private int turnFactor = 3;
	private List<Color> colors = new ArrayList<Color>();
	private Color myColor;
	private List<Boid> neighbors = new ArrayList<Boid>();
	private List<Boid> myFlock = new ArrayList<Boid>();
	private TwoDPoint p;

	private BoidSettings settings;

	public Boid(int w, int h, BoidSettings settings) {
		x = Math.random() * w;
		y = Math.random() * h;
		p = new TwoDPoint(x, y);
		this.w = w;
		this.h = h;
		this.settings = settings;
		dx = Math.random() * 10 - 5;
		dy = Math.random() * 10 - 5;
		flockId = (int)(Math.random() * 5);
		colors.add(Color.red);
		colors.add(Color.white);
		colors.add(Color.orange);
		colors.add(Color.green);
		colors.add(Color.blue);
		myColor = colors.get(flockId);
	}

	public void draw(Graphics2D g) {
		g.setColor(myColor);
		g.drawOval((int)getX(), (int)getY(), size, size);
	}

	public void move(KDTree tree) {
		findNeighbors(tree);
		flyToCenter();
		avoidOthers();
		matchVelocity();
		speedLimit();
		keepInBounds();
		
		x += dx;
		y += dy;
		p = new TwoDPoint(x, y);
		id = (int)(y * h + x);
	}

	private void findNeighbors(KDTree tree) {
		//System.out.println("Range: " + (x - settings.getVisualRange()) + " " + (x + settings.getVisualRange()));
		//System.out.println("Range: " + (y - settings.getVisualRange()) + " " + (y + settings.getVisualRange()));
		myFlock.clear();
		neighbors.clear();
		Iterator<IMultiPoint> boids = tree.range(new Hypercube(
				x - settings.getVisualRange(), 
				x + settings.getVisualRange(),
				y - settings.getVisualRange(),
				y + settings.getVisualRange()
				));
		
		while(boids.hasNext()) {
			Boid b = (Boid)boids.next();
			neighbors.add(b);
			if(b.flockId == flockId) {
				myFlock.add(b);
			}
		}
		
//		if(neighbors.size() > 0) {
//			System.out.println(neighbors.size());
//		}
//		System.exit(-1);
		
	}

	private void matchVelocity() {
		double avgdx = 0;
		double avgdy = 0;

		int ns = 0;

		for(Boid otherBoid: myFlock) {
			avgdx += otherBoid.dx;
			avgdy += otherBoid.dy;
			ns++;
		}

		if(ns > 0) {
			avgdx = avgdx / ns;
			avgdy = avgdy / ns;

			dx += (avgdx - dx) * settings.getMatchFactor();
			dy += (avgdy - dy) * settings.getMatchFactor();
		}

	}

	private void avoidOthers() {
		double mx = 0;
		double my = 0;
		for(Boid otherBoid: neighbors) {
			if(distance(otherBoid) < settings.getSeparation()) {
				mx += x - otherBoid.x;
				my += y - otherBoid.y;
			}
		}
		dx += mx * settings.getAvoidFactor();
		dy += my * settings.getAvoidFactor();

	}

	private void speedLimit() {
		double speed = Math.sqrt((dx * dx) + (dy * dy));
		if(speed > settings.getSpeedLimit()) {
			dx = (dx / speed) * settings.getSpeedLimit();
			dy = (dy / speed) * settings.getSpeedLimit();
		}
	}

	private void flyToCenter() {
		double cx = 0;
		double cy = 0;
		int ns = 0;

		for(Boid otherBoid: myFlock) {
			cx += otherBoid.x;
			cy += otherBoid.y;
			ns++;
		}

		if(ns > 0) {
			cx = cx / ns;
			cy = cy / ns;
			dx += (cx - x) * settings.getCenterFactor();
			dy += (cy - y) * settings.getCenterFactor();
		}

	}

	private double distance(Boid otherBoid) {
		return Math.sqrt(((otherBoid.x - x) * (otherBoid.x - x)) + ((otherBoid.y - y) * (otherBoid.y - y)));
	}

	private void keepInBounds() {
		if(x < margin) dx += settings.getSpeedLimit() / 10;
		if(x > w - margin) dx -= settings.getSpeedLimit() / 10;
		if(y < margin) dy += settings.getSpeedLimit() / 10;
		if(y > h - margin) dy -= settings.getSpeedLimit() / 10;
	}

	public void print() {
		//color = Color.red;
		System.out.println(id + ": X: " + x + " Y: " + y + " DX: " + dx + " DY: " + dy + " W: " + w + " H: " + h);

	}

	@Override
	public int dimensionality() {
		return p.dimensionality();
	}

	@Override
	public double getCoordinate(int dx) {
		return p.getCoordinate(dx);
	}

	@Override
	public double distance(IMultiPoint imp) {
		return p.distance(imp);
	}

	@Override
	public double[] raw() {
		return p.raw();
	}

}
