package net.nilosplace.JavaGame2.boids;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import algs.model.IMultiPoint;
import algs.model.kdtree.KDTree;

public class Main extends JPanel implements ActionListener {

	private int w = 1280;
	private int h = 768;
	private int numberOfBoids = 1000;
	private List<Boid> boids = new ArrayList<Boid>();
	private final BufferedImage bi;
	private final JLabel jl = new JLabel();
	private final Timer t  = new Timer(5, this);
	private BoidSettings settings = new BoidSettings();

	public Main() {
		super(true);
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(w + 30, h + 100));
		bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		jl.setIcon(new ImageIcon(bi));
		this.add(jl);
		
		settings.add(this);
		t.start();
		createBoids();
	}

	private void createBoids() {
		for(int i = 0; i < numberOfBoids; i++) {
			boids.add(new Boid(w, h, settings));
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Graphics2D g = bi.createGraphics();
		g.setColor(Color.black);
		g.fillRect(0, 0, w ,h);
		KDTree newTree = new KDTree(2);
		for(Boid b: boids) {
			newTree.insert(b);
		}
		for(Boid b: boids) {
			b.move(newTree);
			b.draw(g);
		}
		//boids.get(20).print();
		g.dispose();
		jl.repaint();
	}


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new JFrame();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.add(new Main());
				frame.pack();
				frame.setVisible(true);
			}
		});
	}

}
