package net.nilosplace.JavaGame2.boids;

import java.awt.GridLayout;

import javax.swing.*;

import lombok.Data;

@Data
public class BoidSettings {

	private double separation = 20;
	private double visualRange = 75;
	private double speedLimit = 10;
	private double centerFactor = 0.05;
	private double avoidFactor = 0.05;
	private double matchFactor = 0.05;
	
	private JSlider sliderSeparation = new JSlider(JSlider.HORIZONTAL, 0, 100, 20);
	private JSlider sliderVisutalRange = new JSlider(JSlider.HORIZONTAL, 0, 400, 75);
	private JSlider sliderSpeedLimit = new JSlider(JSlider.HORIZONTAL, 1, 50, 10);
	private JSlider sliderCenterFactor = new JSlider(JSlider.HORIZONTAL, 0, 30, 5);
	private JSlider sliderAvoidOthers = new JSlider(JSlider.HORIZONTAL, 0, 50, 5);
	private JSlider sliderMatchFactor = new JSlider(JSlider.HORIZONTAL, 0, 50, 5);

	public BoidSettings() {
		sliderSeparation.addChangeListener(e -> {
			separation = ((JSlider)e.getSource()).getValue();
			print();
		});
		sliderVisutalRange.addChangeListener(e -> {
			visualRange = ((JSlider)e.getSource()).getValue();
			print();
		});
		sliderSpeedLimit.addChangeListener(e -> {
			speedLimit = ((JSlider)e.getSource()).getValue();
			print();
		});
		sliderCenterFactor.addChangeListener(e -> {
			centerFactor = (((JSlider)e.getSource()).getValue() / 100d);
			print();
		});
		sliderAvoidOthers.addChangeListener(e -> {
			avoidFactor = (((JSlider)e.getSource()).getValue() / 100d);
			print();
		});
		sliderMatchFactor.addChangeListener(e -> {
			matchFactor = (((JSlider)e.getSource()).getValue() / 100d);
			print();
		});
	}
	
	private void print() {
		//System.out.println("S: " + separation + " VR: " + visualRange + " SL: " + speedLimit + " CF: " + centerFactor + " AO: " + avoidFactor + " MF: " + matchFactor);
		
	}

	public void add(Main main) {
		JPanel panel = new JPanel(new GridLayout());
		panel.add(new JLabel("Separation"));
		panel.add(sliderSeparation);
		main.add(panel);
		
		panel = new JPanel(new GridLayout());
		panel.add(new JLabel("Visual Range"));
		panel.add(sliderVisutalRange);
		main.add(panel);
		
		panel = new JPanel(new GridLayout());
		panel.add(new JLabel("Speed Limit"));
		panel.add(sliderSpeedLimit);
		main.add(panel);
		
		panel = new JPanel(new GridLayout());
		panel.add(new JLabel("Center Factor"));
		panel.add(sliderCenterFactor);
		main.add(panel);
		
		panel = new JPanel(new GridLayout());
		panel.add(new JLabel("Avoid Others"));
		panel.add(sliderAvoidOthers);
		main.add(panel);
		
		panel = new JPanel(new GridLayout());
		panel.add(new JLabel("Match Speed"));
		panel.add(sliderMatchFactor);
		main.add(panel);

	}
	
	
}
