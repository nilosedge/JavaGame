package net.nilosplace.JavaGame2;

import static org.lwjgl.opengl.GL20.*;

import java.io.*;


public class MandelShader {
	
	int program;
	private int vs;
	private int fs;
	
	public MandelShader() {
		program = glCreateProgram();
		
		vs = glCreateShader(GL_VERTEX_SHADER);
		fs = glCreateShader(GL_FRAGMENT_SHADER);
		
		glShaderSource(vs, readFile("/Users/balrog/git/JavaGame/src/main/resources/shaders/mandelbrol.vs"));
		glShaderSource(fs, readFile("/Users/balrog/git/JavaGame/src/main/resources/shaders/mandelbrol.fs"));
		
		glCompileShader(vs);
		glCompileShader(fs);
		if(glGetShaderi(vs, GL_COMPILE_STATUS) != 1) {
			System.err.println("VS: " + glGetShaderInfoLog(vs));
			System.exit(-1);
		}

		if(glGetShaderi(fs, GL_COMPILE_STATUS) != 1) {
			System.err.println("FS: " + glGetShaderInfoLog(fs));
			System.exit(-1);
		}

		glAttachShader(program, vs);
		glAttachShader(program, fs);

		glLinkProgram(program);
		if(glGetProgrami(program, GL_LINK_STATUS) != 1) {
			System.err.println("LINK: " + glGetProgramInfoLog(program));
			System.exit(-1);
		}
		glValidateProgram(program);
		
		if(glGetProgrami(program, GL_VALIDATE_STATUS) != 1) {
			System.err.println("VALID: " + glGetProgramInfoLog(program));
			System.exit(-1);
		}

	}
	
	private String readFile(String filename) {
		StringBuilder string = new StringBuilder();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line;
			while((line = reader.readLine()) != null) {
				string.append(line + "\n");
			}
			reader.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return string.toString();
	}

}
