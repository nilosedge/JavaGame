package net.nilosplace.JavaGame2;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.IntBuffer;

import org.lwjgl.Version;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

public class GameOfLife {

	private long window;
	private int w = 1600;
	private int h = 1200;
	private boolean[][] board = new boolean[w][h];
	
	public static void main(String[] args) {
		new GameOfLife().run();

	}
	
	public void run() {
		System.out.println("Hello LWJGL " + Version.getVersion() + "!");

		for(int i = 0; i < w; i++) {
			for(int j = 0; j < h; j++) {
				board[i][j] = Math.random() > .9;
			}
		}
		
		init();
		loop();

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}
	
	private void init() {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow(w, h, "Hello World!", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
			System.out.println("Window event: " + key);
		});

		// Get the thread stack and push a new frame
		try ( MemoryStack stack = stackPush() ) {
			IntBuffer pWidth = stack.mallocInt(1); // int*
			IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			glfwGetWindowSize(window, pWidth, pHeight);

			// Get the resolution of the primary monitor
			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window
			glfwSetWindowPos(window, (vidmode.width() - pWidth.get(0)) / 2, (vidmode.height() - pHeight.get(0)) / 2);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);
	}

	private void loop() {
		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();

		// Run the rendering loop until the user has attempted to close
		// the window or has pressed the ESCAPE key.
		while ( !glfwWindowShouldClose(window) ) {

			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			computeLife();
			drawLife();

			glfwSwapBuffers(window); // swap the color buffers
			// Poll for window events. The key callback above will only be
			// invoked during this call.
			glfwPollEvents();
		}
	}

	private void computeLife() {
		int[][] counts = new int[w][h];
		for(int i = 0; i < w; i++) {
			for(int j = 0; j < h; j++) {
				if(i > 0 && j > 0 && board[i - 1][j - 1]) counts[i][j]++;
				if(i > 0 && board[i - 1][j]) counts[i][j]++;
				if(j > 0 && board[i][j - 1]) counts[i][j]++;
				if(i < w - 1 && j < h - 1 && board[i + 1][j + 1]) counts[i][j]++;
				if(i < w - 1 && board[i + 1][j]) counts[i][j]++;
				if(j < h - 1 && board[i][j + 1]) counts[i][j]++;
				if(i > 0 && j < h - 1 && board[i - 1][j + 1]) counts[i][j]++;
				if(i < w - 1 && j > 0 && board[i + 1][j - 1]) counts[i][j]++;
			}
		}
		for(int i = 0; i < w; i++) {
			for(int j = 0; j < h; j++) {
				board[i][j] = (board[i][j] && (counts[i][j] == 2 || counts[i][j] == 3)) || (!board[i][j] && counts[i][j] == 3);
			}	
		}
		
	}

	private void drawLife() {
		glPointSize(1);

		float w_div_2 = (float)w / 2f;
		float h_div_2 = (float)h / 2f;
		
		glBegin(GL_POINTS);		
		{
			//glDrawPixels(w, h, GL_RGB, GL_UNSIGNED_INT, array);
			//glColor3f(255f, 0f, 0f);
			glColor3f(255f, 128f, 0f);
			for(int i = 0; i < w; i++) {
				for(int j = 0; j < h; j++) {
					if(board[i][j]) {
						glVertex2f((((float)i - w_div_2) / w_div_2), (((float)j - h_div_2) / h_div_2));
					}
					
				}
			}
		}
		glEnd();
	}

}
